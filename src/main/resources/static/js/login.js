$(function () {
  const userIdControl = $('#user-id');
  const userPwdControl = $('#user-pwd');

  $('#btn-login').click(function () {
    var id = userIdControl.val();
    var pwd = userPwdControl.val();

    if (!id) {
      alert('아이디를 입력해주세요.');
      return false;
    }

    if (!pwd) {
      alert('비밀번호를 입력해주세요.');
      return false;
    }

    $.ajax({
      url: "/auth",
      data: JSON.stringify({id: id, pwd: pwd}),
      method: "POST",
      contentType:"application/json;charset=UTF-8",
    })
    .done(function(data) {
      alert(data);
    })
    .fail(function(jqXHR, status, err) {
      alert(jqXHR.responseText);
    })
    return false;
  });
  $('#btn-cancel').click(function () {
    userIdControl.val('');
    userPwdControl.val('');
    return false;
  });
});