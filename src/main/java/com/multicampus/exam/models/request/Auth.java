package com.multicampus.exam.models.request;

import lombok.Data;

@Data
public class Auth {
  private String id;
  private String pwd;
}
