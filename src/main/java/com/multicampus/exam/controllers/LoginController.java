package com.multicampus.exam.controllers;

import com.multicampus.exam.models.request.Auth;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

  @RequestMapping(value = {"/", "/login"})
  public String home() {
    return "login";
  }

  @RequestMapping(value = "/auth", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<String> auth(@RequestBody Auth body) {
    if(
        body == null ||
        body.getId() == null ||
        body.getPwd() == null
    ) {
      return new ResponseEntity<String>("잘못된 요청입니다.", HttpStatus.BAD_REQUEST);
    } else if(
        body.getId().equals("abcd") &&
        body.getPwd().equals("1234")
    ) {
      return new ResponseEntity<String>("login ok", HttpStatus.OK);
    }
    return new ResponseEntity<String>("login fail", HttpStatus.UNAUTHORIZED);
  }
}
